package example.weather.weather;

import example.weather.Weather;

import java.io.IOException;
import java.util.Optional;

public interface Query {
    Optional<Weather> execute(WeatherRetrofit api, String apiKey) throws IOException;
}


package example.weather.weather;

import example.weather.Weather;
import retrofit2.Call;

public class QueryBuilder {
    private final WeatherRetrofit weatherRetrofit;
    private final String apiKey;
    private GeoQuery geoQuery;
    private CityQuery cityQuery;
    private String units;
    private String langCode;

    public QueryBuilder(WeatherRetrofit weatherRetrofit, String apiKey) {

        this.weatherRetrofit = weatherRetrofit;
        this.apiKey = apiKey;
    }

    public QueryBuilder withQuery(GeoQuery geoQuery) {
        this.geoQuery = geoQuery;
        return this;
    }

    public QueryBuilder withQuery(CityQuery cityQery) {
        this.cityQuery = cityQery;
        return this;
    }

    public QueryBuilder withUnits(String units) {
        this.units = units;
        return this;
    }

    public QueryBuilder withLangCode(String langCode){
        this.langCode = langCode;
        return this;
    }

    private void validate() {
        if (cityQuery != null && geoQuery != null) {
            throw new IllegalStateException("Cannot pass both queries at once");
        }
        if (cityQuery == null && geoQuery == null) {
            throw new IllegalStateException("Must pass at least one query");
        }

    }
    public Call<Weather> build() {
        validate();
        return weatherRetrofit.weather(
                apiKey,
                cityQuery != null ? cityQuery.toString() : null,
                geoQuery != null ? geoQuery.getLat() : null,
                geoQuery != null ? geoQuery.getLon() : null,
                units,
                langCode
        );
    }

}

package example.weather.weather;


import example.weather.Weather;
import retrofit2.Response;

import java.io.IOException;
import java.util.Optional;

public class GeoQuery implements  Query {
    private final double lat;
    private final double lon;

    public GeoQuery(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }
    public Optional<Weather> execute(WeatherRetrofit api, String apiKey) throws IOException {
        Response<Weather> response = api.weatherByCoord(apiKey, lat, lon).execute();
        if (response.isSuccessful()) {
            return Optional.ofNullable(response.body());
        } else {
            System.err.println(response.errorBody());
            return Optional.empty();
        }
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}

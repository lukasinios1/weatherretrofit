package example.weather.weather;

import example.weather.Weather;
import retrofit2.Response;


import java.io.IOException;
import java.util.Optional;

public class CityQuery implements Query {
    private final String city;
    private final String code;

    public CityQuery(String city) {
        this.city = city;
        this.code = null;
    }

    public CityQuery(String city, String code) {
        this.city = city;
        this.code = code;
    }

    @Override
    public String toString() {
        return city + (code != null ? "," + code : "");
    }

    public Optional<Weather> execute(WeatherRetrofit api, String apiKey) throws IOException {
        Response<Weather> response = api.weatherByQuery(apiKey, toString()).execute();
        if (response.isSuccessful()) {
            return Optional.ofNullable(response.body());
        } else {
            System.err.println(response.errorBody());
            return Optional.empty();
        }
    }
}

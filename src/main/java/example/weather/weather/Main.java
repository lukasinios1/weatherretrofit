package example.weather.weather;

import example.weather.Weather;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.Optional;

public class Main {
    public static void main(String[] args) throws IOException {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WeatherConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WeatherRetrofit weatherRetrofit = retrofit.create(WeatherRetrofit.class);

        while (true){
            callWeatherSync(new QueryBuilder(weatherRetrofit, WeatherConst.API_KEY).withQuery(new CityQuery("Lodz")).withLangCode("pl").withUnits("metric").build()).ifPresent(Main::printWeather);
            beIdle(1);
        }

//        Call<Weather> callAsyn = weatherRetrofit.weatherByQuery(API_KEY, "Lodz", "pl");
//        callWeatherAsyn(callAsyn);
//
//        Call<Weather> call = weatherRetrofit.weatherByQuery(API_KEY, "Lodz", "pl");
//
//        Optional<Weather> ret = callWeatherSync(call);
//        ret.ifPresent(Main::printWeather);
    }

    private static void callWeatherAsyn(Call<Weather> callAsyn) {
        callAsyn.enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                if (response.isSuccessful()) {
                    Main.printWeather(response.body());
                } else {
                    System.err.println(response.errorBody());
                }
            }
            @Override
            public void onFailure(Call<Weather> call, Throwable throwable) {
            }
        });
    }

    private static <T> Optional<T> callWeatherSync(Call<T> call) throws IOException {
        Response<T> response = call.execute();

        if (response.isSuccessful()) {
            return Optional.ofNullable(response.body());
        } else {
            System.err.println(response.errorBody());
            return Optional.empty();
        }

    }

    private static void printWeather(Weather result) {
        System.out.print(result.getName());
        System.out.println(", " + result.getSys().getCountry());
        result.getWeather().forEach(w -> System.out.println(w.getDescription()));
        System.out.println(result.getMain().getTemp()+ "℃");
    }
    private static void beIdle(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

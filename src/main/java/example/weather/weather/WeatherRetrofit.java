package example.weather.weather;

import example.weather.Weather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherRetrofit {
    @GET("data/2.5/weather/")
    Call<example.weather.Weather> weather(
            @Query("appid") String apiKey,
            @Query("q") String query,
            @Query("lat") Double latitude,
            @Query("lon") Double longitude,
            @Query("units") String units,
            @Query("lang") String langCode
    );

    @GET("/data/2.5/weather")
    Call<Weather> weatherByQuery(
            @Query("appid") String apiKey,
            @Query("q") String query
    );

    @GET(WeatherConst.OW_WEATHER)
    Call<Weather> weatherByCoord(
            @Query("appid") String apiKey,
            @Query("lat") double latitude,
            @Query("lon") double longitude
    );

}

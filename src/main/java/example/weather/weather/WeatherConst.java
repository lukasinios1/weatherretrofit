package example.weather.weather;

public class WeatherConst {
    public static final String API_KEY = "58d0070828a865d648f6a01f7cb00fc2";
    public static final String BASE_URL = "http://api.openweathermap.org/";

    public static final String OW_BASE = "http://api.openweathermap.org";
    public static final String OW_WEATHER = "/data/2.5/weather";
}

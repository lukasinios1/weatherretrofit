package example.weather.RxWeather;

import example.weather.Weather;
import example.weather.weather.WeatherConst;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface WeatherRx {
    @GET("data/2.5/weather/")
    Observable<Weather> weather(
            @Query("appid") String apiKey,
            @Query("q") String query,
            @Query("lat") Double latitude,
            @Query("lon") Double longitude,
            @Query("units") String units,
            @Query("lang") String langCode
    );

    @GET("/data/2.5/weather")
    Observable<Weather> weatherByQuery(
            @Query("appid") String apiKey,
            @Query("q") String query
    );

    @GET(WeatherConst.OW_WEATHER)
    Observable<Weather> weatherByCoord(
            @Query("appid") String apiKey,
            @Query("lat") double latitude,
            @Query("lon") double longitude
    );

}

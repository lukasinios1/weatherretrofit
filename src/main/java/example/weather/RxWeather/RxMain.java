package example.weather.RxWeather;

import example.weather.Weather;
import example.weather.weather.CityQuery;
import example.weather.weather.WeatherConst;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.schedulers.Schedulers;

import java.io.IOException;
import java.util.function.Consumer;

public class RxMain {
    public static void main(String[] args) throws IOException {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WeatherConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        WeatherRx weatherRx = retrofit.create(WeatherRx.class);

        while (true) {
            callWeatherAsyn(new RxQueryBuilder(weatherRx, WeatherConst.API_KEY).withQuery(new CityQuery("Lodz")).withLangCode("pl").withUnits("metric").build(),
                    RxMain::printWeather);
            callWeatherAsyn(new RxQueryBuilder(weatherRx, WeatherConst.API_KEY).withQuery(new CityQuery("Zdunska Wola")).withLangCode("pl").withUnits("metric").build(),
                    RxMain::printWeather);
            beIdle(2);
        }

    }

    private static <T> void callWeatherAsyn(Observable<T> callAsyn, Consumer<T> consumer) {
        callAsyn
                .subscribeOn(Schedulers.io())
                .doOnNext(o -> System.out.println(Thread.currentThread().getName()))
                .observeOn(Schedulers.newThread())
                .subscribe(new Observer<T>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        System.err.println("Exception: " + e.getMessage() + ", " + e.getClass() + ", " + e.getCause());
                    }

                    @Override
                    public void onNext(T t) {
                        consumer.accept(t);
                    }
                });

    }

//    private static <T> Optional<T> callWeatherSync(Observable<T> call) throws IOException {
//        Response<T> response = call.execute();
//
//        if (response.isSuccessful()) {
//            return Optional.ofNullable(response.body());
//        } else {
//            System.err.println(response.errorBody());
//            return Optional.empty();
//        }
//
//    }

    private static void printWeather(Weather result) {

        System.out.print(result.getName());
        System.out.println(", " + result.getSys().getCountry());
        result.getWeather().forEach(w -> System.out.println(w.getDescription()));
        System.out.println(result.getMain().getTemp() + "℃" + "\n");
    }

    private static void beIdle(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

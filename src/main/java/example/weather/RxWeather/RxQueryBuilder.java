package example.weather.RxWeather;

import example.weather.Weather;
import example.weather.weather.CityQuery;
import example.weather.weather.GeoQuery;
import rx.Observable;

public class RxQueryBuilder {
    private final WeatherRx weatherRx;
    private final String apiKey;
    private GeoQuery geoQuery;
    private CityQuery cityQuery;
    private String units;
    private String langCode;

    public RxQueryBuilder(WeatherRx weatherRx, String apiKey) {

        this.weatherRx = weatherRx;
        this.apiKey = apiKey;
    }

    public RxQueryBuilder withQuery(GeoQuery geoQuery) {
        this.geoQuery = geoQuery;
        return this;
    }

    public RxQueryBuilder withQuery(CityQuery cityQery) {
        this.cityQuery = cityQery;
        return this;
    }

    public RxQueryBuilder withUnits(String units) {
        this.units = units;
        return this;
    }

    public RxQueryBuilder withLangCode(String langCode){
        this.langCode = langCode;
        return this;
    }

    private void validate() {
        if (cityQuery != null && geoQuery != null) {
            throw new IllegalStateException("Cannot pass both queries at once");
        }
        if (cityQuery == null && geoQuery == null) {
            throw new IllegalStateException("Must pass at least one query");
        }

    }
    public Observable<Weather> build() {
        validate();
        return weatherRx.weather(
                apiKey,
                cityQuery != null ? cityQuery.toString() : null,
                geoQuery != null ? geoQuery.getLat() : null,
                geoQuery != null ? geoQuery.getLon() : null,
                units,
                langCode
        );
    }

}
